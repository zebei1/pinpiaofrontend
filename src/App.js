import './App.css'
import { Outlet } from "react-router-dom";
import MenuModel from './components/MenuModel'

function App() {
  return (

    <div className="App">
      <MenuModel></MenuModel>
      <Outlet></Outlet>
    </div>
  )
}

export default App
