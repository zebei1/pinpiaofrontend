import api from './http'

const getCinemaList = async () => {
  const { data } = await api.get('/cinemas')
  return data
}

const updateCinema = async (id) => {
  const { data } = await api.post(`/cinemas/${id}`)
  return data
}


export {
  getCinemaList,
  updateCinema
}
