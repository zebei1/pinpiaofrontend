import axios from 'axios'

let BASE_URL = 'http://localhost:8080/'

const instance = axios.create({
  headers: {
    'Content-Type': 'application/json;charset=UTF-8',
  },
  baseURL: BASE_URL,
  timeout: 15000,
})

export default instance
