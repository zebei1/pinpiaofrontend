import api from './http'

const getMovieListWithKey = async () => {
  const { data } = await api.get('/movies/key')
  return data
}


export {
  getMovieListWithKey
}
