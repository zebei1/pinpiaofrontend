import React, { useState } from 'react'
import { Button, Modal, Form, Input } from 'antd'
const FormModal = (props) => {
  const [confirmLoading, setConfirmLoading] = useState(false)

  const handleOk = () => {
    props.changeOpenModalStatus(false)
    setConfirmLoading(true)
    setTimeout(() => {
      // setOpen(false);
      setConfirmLoading(false);
    }, 2000);
  };
  const handleCancel = () => {
    props.changeOpenModalStatus(false)
  }
  const onFinish = (values) => {
    console.log('Success:', values);
  }
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  }
  return (
    <>
      <Modal
        title="Add cinema"
        open={props.open}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-start'
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Cinema Name"
            name="cinemaName"
            rules={[
              {
                required: true,
                message: 'Please input cinema name!',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="City"
            name="city"
            rules={[
              {
                required: true,
                message: 'Please input city!',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Address"
            name="adress"
            rules={[
              {
                required: true,
                message: 'Please input adress!',
              },
            ]}
          >
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
export default FormModal;