import React, { useState } from 'react';
import { AppstoreOutlined, MailOutlined, SettingOutlined, PlaySquareOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';

const MenuModel = () => {
  const rootSubmenuKeys = ['sub1', 'sub2', 'sub4'];
  const [openKeys, setOpenKeys] = useState(['sub1']);
  const onOpenChange = (keys) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };
  function getItem(label, key, icon, children, type) {
    return {
      key,
      icon,
      children,
      label,
      type,
    };
  }
  const items = [
    getItem('放映信息', 'sub1', <PlaySquareOutlined />, [
      getItem((<Link to='/movies'>热映电影</Link>), '1'),
    ]),
    getItem('影片管理', 'sub2', <MailOutlined />, [
      getItem((<Link to='/movies'>影片列表</Link>), '2'),
    ]),
    getItem('影院管理', 'sub3', <AppstoreOutlined />, [
      getItem((<Link to='/cinemas'>影院列表</Link>), '4'),
      getItem((<Link to='/videoHalls'>放映厅管理</Link>), '5'),
      getItem('Option 5', '6'),
      getItem('Option 6', '7'),
    ]),
    getItem('订单管理', 'sub4', <SettingOutlined />, [
      getItem((<Link to='/orders'>订单列表</Link>), '8'),
      getItem('Option 9', '9'),
      getItem('Option 10', '10'),
      getItem('Option 11', '11'),
      getItem('Option 12', '12'),
    ]),
  ];

  return (
    <Menu
      mode="inline"
      openKeys={openKeys}
      onOpenChange={onOpenChange}
      style={{
        width: 256,
      }}
      items={items}
    />
  )
}

export default MenuModel
