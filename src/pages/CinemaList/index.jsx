import React, { useEffect, useState } from 'react';
import './index.css'
import { Button, Space, Table } from 'antd';
import { getCinemaList, updateCinema } from '../../api/cinemaApi'
import FormModal from '../../components/FormModal'
const CinemaList = () => {
  const [cinemaList, setCinemaList] = useState([])
  const { Column } = Table;
  const [open, setOpen] = useState(false);
  useEffect(() => {
    const getCinemas = async () => {
      const data = await getCinemaList()
      const dataWithKey = data.map(item => {
        item['key'] = item.id
        return item
      })
      setCinemaList(dataWithKey)
    }
    getCinemas()
  }, [])
  const handleEdit = (record) => {
    console.log(record)
    setOpen(true)
  }
  const handleDelete = (record) => {
    console.log(record)
  }
  const HandleOpenModal = (status) => {
    setOpen(status)
  }
  return (
    <div className='cinema-list-container'>
      <div className='option'>
        <Button
          type="primary"
          style={{
            marginBottom: 16,
          }}
        >
          Add
        </Button>
      </div>
      <Table
        className='cinema-table'
        dataSource={cinemaList}>
        <Column title="CinemaName" dataIndex="cinemaName" key="cinemaName" />
        <Column title="Address" dataIndex="address" key="address" />
        <Column title="City" dataIndex="city" key="city" />
        <Column
          title="Action"
          key="action"
          render={(_, record) => (
            <Space size="middle">
              <Button type="primary" style={{ backgroundColor: 'orange' }} onClick={() => { handleEdit(record) }}>Edit</Button>
              <Button type="primary" style={{ backgroundColor: 'red' }} onClick={() => { handleDelete(record) }}>Delete</Button>
            </Space>
          )}
        />
      </Table>
      <FormModal open={open} changeOpenModalStatus={HandleOpenModal}></FormModal>
    </div>
  )
}

export default CinemaList