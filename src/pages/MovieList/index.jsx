import React, { useEffect, useState } from 'react';
import './index.css'
import { Button, Space, Table } from 'antd';
import { getMovieListWithKey } from '../../api/movieApi'
const MovieList = () => {
  const [movieList, setMovieList] = useState([])
  const { Column } = Table;
  useEffect(() => {
    const getMovieList = async () => {
      const data = await getMovieListWithKey()
      setMovieList(data)
    }
    getMovieList()
  }, [])
  return (
    <div className='movie-list-container'>
      <div className='option'>
        <Button
          type="primary"
          style={{
            marginBottom: 16,
          }}
        >
          Add
        </Button>
      </div>
      <Table
        className='movie-table'
        dataSource={movieList}>
        <Column
          title="Img"
          dataIndex="imgUrl"
          key="imgUrl"
          render={value => (<img src={value} alt='pic' style={{ width: '80px', height: '110px' }}></img>)} />
        <Column title="Name" dataIndex="name" key="name" />
        <Column title="Type" dataIndex="type" key="type" />
        <Column title="Director" dataIndex="director" key="director" />
        <Column title="Actors" dataIndex="actors" key="actors" />
        <Column title="ReleaseDate" dataIndex="releaseDate" key="releaseDate" />
        <Column title="Duration" dataIndex="duration" key="duration" />
        <Column title="Description" dataIndex="description" key="description" />
        <Column
          title="Action"
          key="action"
          render={(_, record) => (
            <Space size="middle">
              <Button type="primary" style={{ backgroundColor: 'orange' }}>Edit</Button>
              <Button type="primary" style={{ backgroundColor: 'red' }}>Delete</Button>
            </Space>
          )}
        />
      </Table>
    </div>
  )
}

export default MovieList



