
import App from '../App'
import MovieList from '../pages/MovieList'
import CinemaList from '../pages/CinemaList'

const routers = [
  {
    path: '/',
    element: <App></App>,
    children: [
      {
        // index: true,
        path: '/movies',
        element: <MovieList></MovieList>
      },
      {
        // index: true,
        path: '/cinemas',
        element: <CinemaList></CinemaList>
      },
    ]
  },
]


export default routers